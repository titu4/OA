package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice2;

public class StringUtilsTest {
    public static void main(String[] args) {
        System.out.println("Task 1: ");
        String a = "Hello";
        String b = "world";
        System.out.println("Input strings: 1)" + a + " 2)" + b);
        System.out.println("Result string: " +
                StringUtils.concatWithoutFirstChars(a,b));

        System.out.println("\nTask 2: ");
        a = "Hello";
        System.out.println("Input string: " + a);
        System.out.println("Result string: " +
                StringUtils.makeStrThreeCharLength(a));

        System.out.println("\nTask 3: ");
        a = "Hello";
        System.out.println("Input string: " + a);
        System.out.println("Result string: " +
                StringUtils.lastTwoCharToStart(a));

        System.out.println("\nTask 3: ");
        a = "Hello";
        System.out.println("Input string: " + a);
        System.out.println("Result string: " +
                StringUtils.lastTwoCharToStart(a));

        System.out.println("\nTask 4: ");
        a = "Hello";
        System.out.println("Input string: " + a);
        System.out.println("Result string: " +
                StringUtils.doubleChars(a));

        System.out.println("\nTask 5: ");
        a = "bob is bab";
        System.out.println("Input string: " + a);
        System.out.println("count: " +
                StringUtils.findMatches(a));

        System.out.println("\nTask 5: ");
        a = "bob is bab";
        System.out.println("Input string: " + a);
        System.out.println("count: " +
                StringUtils.findMatches(a));

        System.out.println("\nTask 6:");
        String s = "th*is is sum*mer";
        System.out.println(" Input string: " + s);
        System.out.println("Result string: " +
                StringUtils.removeCharNearAsterisk( s));

        System.out.println("\nTask 7: ");
        a = "boa iS Bab aNds";
        System.out.println("Input string: " + a);
        System.out.println("count: " +
                StringUtils.countEndMatches(a));

        System.out.println("\nTask 8: ");
        a = "Hello world!";
        b = "world";
        System.out.println("Input strings: 1)" + a + " 2)" + b);
        System.out.println("Result string: " +
                StringUtils.removeDouplicates(a,b));

    }
}
