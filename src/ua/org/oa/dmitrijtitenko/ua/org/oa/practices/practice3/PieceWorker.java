package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

public class PieceWorker extends Employee{
    protected int totalPieces;

    protected float pieceWage = 0.1f;

    public PieceWorker(String firstName, String secondName, int pieces_) {
        super(firstName, secondName);

        totalPieces = pieces_;
    }

    @Override
    public float calcTotalYearSalary() {
        return totalPieces*pieceWage;
    }

    @Override
    public float calcMonthSalary(int m) {
        return 0;
    }
}
