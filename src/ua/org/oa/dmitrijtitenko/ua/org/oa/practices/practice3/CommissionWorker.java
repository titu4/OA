package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

public class CommissionWorker extends Employee {
    protected int sales;

    protected int baseWage = 1000;
    protected float salesPercentage = 3.5f;

    public CommissionWorker(String firstName, String secondName, int sales_) {
        super(firstName, secondName);
        sales = sales_;
    }

    @Override
    public float calcTotalYearSalary() {
        return baseWage + (sales/100)*salesPercentage;
    }

    @Override
    public float calcMonthSalary(int m) {
        return 0;
    }

}
