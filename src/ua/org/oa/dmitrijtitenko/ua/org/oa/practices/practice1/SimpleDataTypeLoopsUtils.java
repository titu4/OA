package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice1;

public class SimpleDataTypeLoopsUtils {

    //task1
    public static byte f_byte;
    public static short f_short;
    public static int f_int;
    public static long f_long;
    public static float f_float;
    public static double f_double;
    public static char f_char;
    public static boolean f_boolean;

    public static void printPrimitives(){
        System.out.println("Non initialized class fields:");
        System.out.println("\tbyte: " + f_byte);
        System.out.println("\tshort: " + f_short);
        System.out.println("\tint: " + f_int);
        System.out.println("\tlong: " + f_long);
        System.out.println("\tfloat: " + f_float);
        System.out.println("\tdouble: " + f_double);
        System.out.println("\tchar: " + f_char);
        System.out.println("\tboolean: " + f_boolean);

        System.out.println("\nLocal variables:");
        byte localVar_byte = 0;
        short localVar_short = 0;
        int localVar_int = 0;
        long localVar_long = 0;
        float localVar_float = 0;
        double localVar_double = 0;
        char localVar_char = 0;
        boolean localVar_boolean = false;
        System.out.println("\tbyte: " + localVar_byte);
        System.out.println("\tshort: " + localVar_short);
        System.out.println("\tint: " + localVar_int);
        System.out.println("\tlong: " + localVar_long);
        System.out.println("\tfloat: " + localVar_float);
        System.out.println("\tdouble: " + localVar_double);
        System.out.println("\tchar: " + localVar_char);
        System.out.println("\tboolean: " + localVar_boolean);
    }
    
    //task2
    public static void printInitializedFloat() {
        float f1 = (float) 1.;
        System.out.println("1. = " + f1);

        float f2 = (float) 1;
        System.out.println("1 = " + f2);

        float f3 = (float) 0x1;
        System.out.println("0x1 = " + f3);

        float f4 = (float) 0b1;
        System.out.println("0b1 = " + f4);

        float f5 = (float) 1.0e1;
        System.out.println("1.0e1 = " + f5);

        float f6 = (float) 01;
        System.out.println("01 = " + f6);
    }

    //task3
    public static void printInitializedShort() {
        short s1 = (short) (15500 + 26001);
        System.out.println("Sum of " + 15500 + " and " + 26001 + " stored in short: " + s1);

        short s2 = (short) (5 + .5);
        System.out.println("Sum of " + 5 + " and " + .5 + " stored in short: " + s2);

        int a1 = 5;
        float b1 = (float) .5;
        short s3 = (short) (a1 + b1);
        System.out.println("Sum of (int) " + a1 + " and (float) " + b1 + " stored in short: " + s3);

        byte a2 = 5;
        short b2 = 100;
        short s4 = (short) (a2 + b2);
        System.out.println("Sum of (byte) " + a2 + " and (short) " + b2 + " stored in short: " + s4);

        float a3 = (float) 5.123;
        double b3 = .9999999;
        short s5 = (short) (a3 + b3);
        System.out.println("Sum of (float) " + a3 + " and (double) " + b3 + " stored in short: " + s5);
    }

    //task4
    public static void isRightTriangle(int cathetus1, int cathetus2, int hypotenuse) {
        double catSum = cathetus1+cathetus2;
        if(cathetus1>0 &&
                cathetus2>0 &&
                hypotenuse>0 &&
                catSum>hypotenuse){
            boolean isRightTriangle = (hypotenuse == Math.sqrt (cathetus1*cathetus1 + cathetus2*cathetus2) )?true:false;
            System.out.println("Cathetus 1: " + cathetus1);
            System.out.println("Cathetus 2: " + cathetus2);
            System.out.println("Hypotenuse: " + hypotenuse);
            System.out.println("Is triangle right : " + ( (isRightTriangle)?"Yes":"No") );
        }else {
            System.out.println("Wrong arguments");
        }
    }

    //task5
    public static void sumFromTo(int from, int to) {
        if(from<to) {
            double sum = 0;

            for (int i = from; i <= to; i++) {
                sum += i;
            }

            System.out.println("Sum of numbers in range from " + from + " to " + to + " = " + sum);
        }else {
            System.out.println("Wrong arguments");
        }
    }

    //task6
    public static void sumEveNumber(int from, int to) {
        if(from<to) {
            double sum = 0;

            for (int i = from; i <= to; i++) {
                if(i%2 == 0) {
                    sum += i;
                }
            }

            System.out.println("Sum of even numbers in range from " + from + " to " + to + " = " + sum);
        }else {
            System.out.println("Wrong arguments");
        }
    }

    //task7
    public static void sumSimpleNumbers(int from, int to) {
            double sum = 0;
        if(from<to) {
            iLoop:for (int i=from; i<to ;i++){
                if(i==1) {
                    continue iLoop;
                }
                for(int j=2; j<=Math.sqrt(i) ;j++){
                    if(i%j == 0) {
                        continue iLoop;
                    }
                }

                sum += i;
            }

            System.out.println("Sum of simple numbers in range from " + from + " to " + to + " = " + sum);
        }else {
            System.out.println("Wrong arguments");
        }
    }

    //task8
    public static void checkIfSumOfTwoEqualThird(int i, int i1, int i2) {
        if ((i+i1) == i2){
            System.out.println(i + " + " + i1 + " = " + i2);
            System.out.println(true);
        }else if ((i+i2) == i1){
            System.out.println(i + " + " + i2 + " = " + i1);
            System.out.println(true);
        }else if ((i1+i2) == i){
            System.out.println(i1 + " + " + i2 + " = " + i);
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }

    //task9
    public static double averageValue(int a, int b){
        double result = 0;

        if(a>0 && b>0 && a>b){
            for(int i=a; i>=b ;i--){
                result += i;
            }
            result = result/(a - b + 1);
        }else {
            System.out.println("Wrong arguments");
        }

        return result;
    }

    public static void calcCreditPayments(double creditValue, double percentageValue, int monthValue) {
        System.out.println("You are going to take " + creditValue + "uah");
        System.out.println("for " + monthValue + " month");
        System.out.println("Under " + percentageValue + "% per year");

        double monthlyPayment = creditValue/monthValue;
        double leftSumToPay = creditValue;
        double charge = 0;

        double totalCharge = 0;

        for (int i=1; i<=monthValue ;i++){
            System.out.print(i + ") "); //print month number

            charge = (leftSumToPay/100) * (percentageValue/12); //calc this month charge

            System.out.println("You need pay this month : " + (monthlyPayment + charge) );
            System.out.println("base : " + monthlyPayment);
            System.out.println("charge : " + charge);
            System.out.println("-----");

            leftSumToPay -= monthlyPayment;

            System.out.println("left to pay : " + leftSumToPay);
            System.out.println();

            totalCharge += charge;
        }

        System.out.println("This cerdit will cost you: " + totalCharge + "uah");
    }
}
