package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

public class StudentException extends Exception{

    public StudentException(String message) {
        super(message);
    }
}
