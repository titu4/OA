package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

public class StudentAgeException extends StudentException {
    public StudentAgeException(String message) {
        super(message);
    }
}
