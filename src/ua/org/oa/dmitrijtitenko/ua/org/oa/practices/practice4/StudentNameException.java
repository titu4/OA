package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

public class StudentNameException extends StudentException {
    public StudentNameException(String message) {
        super(message);
    }
}
