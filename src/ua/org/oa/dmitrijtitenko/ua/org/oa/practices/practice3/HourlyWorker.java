package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

public class HourlyWorker extends Employee{
    protected int baseHours;
    protected int hightHours;

    protected int baseWage = 10;
    protected int highWage = 15;

    public HourlyWorker(String firstName, String secondName, int baseHours_, int hightHours_) {
        super(firstName, secondName);
        baseHours = baseHours_;
        hightHours = hightHours_;
    }

    @Override
    public float calcTotalYearSalary() {
        return baseHours*baseWage + hightHours*highWage;
    }

    @Override
    public float calcMonthSalary(int m) {
        return 0;
    }
}
