package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice2.hw;

public class App {
    public static void main(String[] args) {
        MonthlyTemperature january = new MonthlyTemperature();

        System.out.println("\n---------\n" + january.toString());

        System.out.println(january.getMonthName() + " average temp: " + january.getAverageTemp());
        System.out.println("max temp: " + january.getMaxTemp());
        System.out.println("min temp: " + january.getMinTemp());
    }
}
