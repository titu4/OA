package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

public class App {
    public static void main(String[] args) {
        Group g1 = new Group("G1",2);

        g1.addSudent(new Student("Dima Titenko", 27));
        int id = g1.addSudent(new Student("Vasaj Pertichenko", 17));
        g1.removeStudent(id);
        g1.removeStudent("Dima Titenko");

        g1.addSudent(new Student("Maxim Ivanov", 37));
        g1.addSudent(new Student("Oleg Petrov", 37));

        g1.print();

        Group g2 = new Group("G2",3);

        g2.fillGroup();

        g2.print();

    }
}
