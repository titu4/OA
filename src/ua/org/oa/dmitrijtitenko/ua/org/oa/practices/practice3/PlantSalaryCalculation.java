package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

import java.util.ArrayList;
import java.util.List;

public class PlantSalaryCalculation {
    public static void main(String[] args) {
        List<Employee> employees = new ArrayList<>();

        employees.add(
                new Boss("Ivan", "Ivanov")
                        .setWorkedWeeks(1,3)
                        .setWorkedWeeks(2,3)
                        .setWorkedWeeks(3,1)
                        .setWorkedWeeks(4,0));

        employees.add(
                new Boss("Mikhail", "Ivanov")
                        .setWorkedWeeks(1,2)
                        .setWorkedWeeks(2,0)
                        .setWorkedWeeks(3,1));

        employees.add(new HourlyWorker("Petr","Petrov",80, 3));
        employees.add(new HourlyWorker("Ivan","Petrov",80, 3));
        employees.add(new HourlyWorker("Serg","Petrov",80, 3));

        employees.add(new PieceWorker("Kostya","Sidorov",1000));

        employees.add(new CommissionWorker("Lena","Ivanova",10_000));

        float totalPayments = 0;

        for (Employee emp:employees){
            System.out.println(emp.toString());
            System.out.println("Total year salary:" + emp.calcTotalYearSalary() + "\n");

            totalPayments += emp.calcTotalYearSalary();
        }

        System.out.println("----");
        System.out.println(totalPayments);

        int m = 2;
        for (Employee emp:employees){
            System.out.println(emp.toString());
            System.out.println(m + " month salary:" + emp.calcMonthSalary(m) + "\n");

            totalPayments += emp.calcTotalYearSalary();
        }
    }
}
