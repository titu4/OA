package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

public class Boss extends Employee {
    protected int wage = 1000;

    protected int[] monthly = new int[12];

    public Boss(String firstName, String secondName) {
        super(firstName, secondName);
    }

    @Override
    public float calcTotalYearSalary() {
        float sal = 0;

        for (int i=0;i<12;i++) {
            sal += wage * monthly[i];
        }

        return sal;
    }

    @Override
    public float calcMonthSalary(int m) {
        float sal = 0;

        sal += wage * monthly[m-1];

        return sal;
    }

    public Boss setWorkedWeeks(int month,int weeks){

        if (13>month && 0<month)
            monthly[month-1] = weeks;

        return this;
    }
}
