package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice2;

public class StringUtils {
    public static String removeCharNearAsterisk(String s){
        String[] ws = s.split(" ");

        StringBuilder builder = new StringBuilder();
        for (String w : ws){
            StringBuilder sb = new StringBuilder(w);
            int index = 0;
            while ((index = sb.indexOf("*",index)) != -1){
                if(index == 0) {
                    sb.delete(0, index + 2);
                }else if (index == sb.length() - 1){
                    sb.delete(sb.length()-1, index + 1);
                }else {
                    sb.delete(index-1, index+2);
                    index--;
                }
            }
            builder.append(sb.toString() + " ");
        }

        return builder.toString();
    }

    public static String concatWithoutFirstChars(String a, String b) {
        StringBuilder sba = new StringBuilder(a);
        StringBuilder sbb = new StringBuilder(b);

        sba.delete(0,1);
        sbb.delete(0,1);

        return sba
                .append(sbb.toString())
                .toString();
    }

    public static String makeStrThreeCharLength(String s) {
        StringBuilder sb = new StringBuilder(s);

        if (s.length() < 3){
            System.out.println("Error, too short string!");
            return s;
        }else if (s.length() == 3){
            return s;
        }else if (s.length()%2 == 0){
            System.out.println("Error, string is even!");
            return s;
        }
        else {
            while (sb.length()>3){
                sb.delete(0,1);
                sb.delete(sb.length()-1,sb.length());
            }
        }

        return sb.toString();
    }

    public static String lastTwoCharToStart(String a) {
        StringBuilder sb = new StringBuilder(a);

        char[] twoChar = new char[2];
        sb.getChars(sb.length()-2, sb.length(), twoChar, 0);
        sb.insert(0, twoChar);
        sb.delete(sb.length()-2, sb.length());

        return sb.toString();
    }

    public static String doubleChars(String a) {
        StringBuilder sb = new StringBuilder(a);

        for (int i = 0; i < sb.length(); i+=2) {
            sb.insert(i,sb.charAt(i));
        }

        return sb.toString();
    }

    public static int findMatches(String a) {
        if (a.length() < 3)
            return -1;

        StringBuilder sb = new StringBuilder(a);

        int count = 0;
        for (int i = 0; i < sb.length()-2; i++) {
            if (sb.charAt(i) == 'b' && sb.charAt(i+2) == 'b')
                count++;
        }

        return count;
    }

    public static int countEndMatches(String a) {
        int count = 0;
        String upp = a.toUpperCase();
        String[] arr = upp.split(" ");

        for (String s : arr) {
            int lastChar = s.length() - 1;

            if (Character.compare(s.charAt(lastChar),'A') == 0
                    || Character.compare(s.charAt(lastChar),'S') == 0) {
                count++;
            }
        }

        return count;
    }

    public static String removeDouplicates(String a, String b) {
        StringBuilder sb = new StringBuilder(a);

        int index = 0;
        while ((index = sb.indexOf(b)) != -1) {
            sb.delete(index,
                    index+b.length());
        }

        return sb.toString();
    }
}
