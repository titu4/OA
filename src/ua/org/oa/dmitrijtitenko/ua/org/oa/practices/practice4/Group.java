package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

public class Group {

    private Student[] students;
    private int groupSize;
    private String groupName;

    private int i = 0;

    public Group(String groupName, int n) {
        setGroupSize(n);
        students = new Student[getGroupSize()];

        setGroupName(groupName);
    }

    private int add(Student s) throws Exception {
        if (i < getGroupSize() ) {
            if(search(s.getName()) < 0)
                students[i] = s;
            else
                throw new Exception("Student is already in group");
        }else {
            System.out.println("Group is complete");
        }
        return i++;
    }

    public int addSudent(){
        int id = -1;

        while (true) {
            try {
                id = this.add(Student.createStudentConsole());
                break;
            } catch (Exception e) {
                System.out.println("\t-->" + e.getMessage());
                System.out.println("\tTry one more time");
            }
        }

        return id;
    }

    public int addSudent(Student s){
        int id = -1;
        try {
            id = add(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    public int addSudent(Student s,int id){
        students[id] = s;

        return id;
    }

    public void fillGroup() {
        for (int j = 0; j < getFilledGroupSize(); j++) {
            while (true) {
                try {
                    this.add(Student.createStudentConsole());
                    break;
                } catch (Exception e) {
                    System.out.println("\t-->" + e.getMessage());
                    System.out.println("\tTry one more time");
                }
            }
        }
    }

    private int getFilledGroupSize() {
        return students.length;
    }

    public void print() {
        System.out.println("\nGroup: " + getGroupName());
        for (Student student : students) {
            System.out.println("\t" + student.toString());
        }
    }

    public int search(String name){
        int id = -1;

        int iter = 0;
        for (Student student : students) {
            if (student != null && student.getName().equals(name))
                id = iter;

            iter++;
        }

        return id;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }


    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public void removeStudent(String lName) {
        removeStudent(search(lName));
    }

    public void removeStudent(int id) {
        students[id] = null;
        i--;

        //shift all student left
        for (int j = id; j < getGroupSize()-1; j++) {
            students[id] = students[id+1];
        }

    }
}
