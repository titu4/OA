package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Student {
    private static final int MAX_AGE = 130;
    private static final int MIN_AGE = 15;
    private static final int NAME_MIN_LENGHT = 3;

    private String name;
    private int age;

    public Student(String name, int age) {
        try {
            setName(name);
            setAge(age);
        } catch (StudentException e) {
            e.printStackTrace();
        }
    }

    public Student() {}

    static Student createStudentConsole() {
        Student student = new Student();

        try(BufferedReader br = new BufferedReader(new InputStreamReader(new UncloseInputStream(System.in)))){

            System.out.print("Input new student's name: ");
            String name_ = br.readLine();

            System.out.print("Input new student's age: ");
            int age_ = Integer.parseInt(br.readLine());

            while (true){
                try {
                    student.setName(name_);
                    student.setAge(age_);
                    break;
                }catch (StudentNameException ex){
                    System.out.println("\t-->" + ex.getMessage());
                    System.out.println("\tTry one more time");

                    System.out.print("Input new student's name: ");
                    name_ = br.readLine();
                } catch (StudentException ex) {
                    System.out.println("\t-->" + ex.getMessage());
                    System.out.println("\tTry one more time");

                    System.out.print("Input new student's age: ");
                    age_ = Integer.parseInt(br.readLine());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return student;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) throws StudentNameException {
        if(name.length() >= NAME_MIN_LENGHT)
            this.name = name;
        else
            throw new StudentNameException("Student's name is too short, please insert name longer than " + NAME_MIN_LENGHT);

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws StudentAgeException {
        if(age >= MIN_AGE && age <= MAX_AGE)
            this.age = age;
        else
            throw new StudentAgeException("Student age should be in range from:" + MIN_AGE + " to:" + MAX_AGE);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if ( ( this.getName().equals(((Student)obj).getName()) )
                && ( this.getAge() == ((Student)obj).getAge() )
        )
            return true;
        else
            return false;
    }

    @Override
    public String toString() {
        return "Student{" +
                "  name='" + name + '\'' +
                ", age=" + age +
                '}';
    }
}
