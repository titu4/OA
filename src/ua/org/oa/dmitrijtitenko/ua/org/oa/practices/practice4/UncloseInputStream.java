package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice4;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public class UncloseInputStream extends FilterInputStream {

    public UncloseInputStream(InputStream in) {
        super(in);
    }

    @Override
    public void close() throws IOException {
//        super.close();
    }
}
