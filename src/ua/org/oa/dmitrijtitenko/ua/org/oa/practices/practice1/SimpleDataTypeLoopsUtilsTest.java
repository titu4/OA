package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice1;

public class SimpleDataTypeLoopsUtilsTest {
    public static void main(String[] args) {
        System.out.println("--- Test for 1th task ---");
        SimpleDataTypeLoopsUtils.printPrimitives();

        System.out.println("\n--- Test for 2th task ---");
        SimpleDataTypeLoopsUtils.printInitializedFloat();

        System.out.println("\n--- Test for 3th task ---");
        SimpleDataTypeLoopsUtils.printInitializedShort();

        System.out.println("\n--- Test for 4th task ---");
        SimpleDataTypeLoopsUtils.isRightTriangle(2, 2, 3);

        System.out.println("\n--- Test for 5th task ---");
        SimpleDataTypeLoopsUtils.sumFromTo(1, 20);

        System.out.println("\n--- Test for 6th task ---");
        SimpleDataTypeLoopsUtils.sumEveNumber(1,20);

        System.out.println("\n--- Test for 7th task ---");
        SimpleDataTypeLoopsUtils.sumSimpleNumbers(1,20);

        System.out.println("\n--- Test for 8th task ---");
        SimpleDataTypeLoopsUtils.checkIfSumOfTwoEqualThird(2,3,1);

        System.out.println("\n--- Test for 9th task ---");
        int a = 5;
        int b = 1;
        double result = SimpleDataTypeLoopsUtils.averageValue(a, b);
        System.out.println("Average value of " + a + " and " + b + " is : " + result);

        System.out.println("\n--- Test for 10th task ---");
        SimpleDataTypeLoopsUtils.calcCreditPayments(12000,12,12);

    }
}
