package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice3;

public abstract class Employee {
    private String firstName;
    private String secondName;

    public Employee(String firstName, String secondName) {
        this.firstName = firstName;
        this.secondName = secondName;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                '}';
    }

    public abstract float calcTotalYearSalary();
    public abstract float calcMonthSalary(int m);
}
