package ua.org.oa.dmitrijtitenko.ua.org.oa.practices.practice2.hw;

import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Scanner;

public class MonthlyTemperature {
    private static final int MIN_MONTH = 1;
    private static final int MAX_MONTH = 12;

    private static final int MIN_YEAR = 1900;
    private static final int MAX_YEAR = 2017;

    private static final float MIN_TEMP = -50;
    private static final float MAX_TEMP = 50;


    private int year;
    private int month;
    private int days;

    private ArrayList<Float> temp = new ArrayList<>();

    public MonthlyTemperature(){
        Scanner scanner = new Scanner(System.in);
        inputYear(scanner);
        inputMonth(scanner);
        inputTemperature(scanner);
    }

    private void inputMonth(Scanner scanner){
        int m;
        System.out.printf("Month: ");

        while (true) {
            m = Integer.parseInt(scanner.nextLine());
            if(m>=MIN_MONTH && m<=MAX_MONTH) {
                break;
            }
            System.out.printf("\t-> month number should be in range from 1 to 12\n" +
                    "please input month number one more time: ");
        }

        setMonth(m);
    }

    private void inputYear(Scanner scanner){
        int y;

        System.out.printf("Year: ");

        while (true) {
            y = Integer.parseInt(scanner.nextLine());
            if(y>MIN_YEAR && y<=MAX_YEAR) {
                break;
            }
            System.out.printf("\t-> Year should be in range from " + MIN_YEAR + " to " + MAX_YEAR + "\n" +
                    "please input year one more time: ");
        }

        setYear(y);
    }

    private void inputTemperature(Scanner scanner) {
        float t;

        for (int i = 1; i <= days; i++) {
            System.out.printf(getMonthName() + " " + i + ": ");

            while (true) {
                t = Float.parseFloat(scanner.nextLine());

                if (t > MIN_TEMP && t <= MAX_TEMP) {
                    break;
                }
                System.out.printf("\t-> temperature should be in range from " + MIN_TEMP + " to " + MAX_TEMP + "\n" +
                        "please input temp one more time: ");
            }
            temp.add(t);
        }

    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
        setDays(
                YearMonth.of(year,month)
                        .lengthOfMonth()
        );
    }

    public float getAverageTemp() {
        float sum = 0;

        for (float t:temp){
            sum += t;
        }

        return sum/days;
    }

    public String getMaxTemp() {
        float max = MIN_TEMP;
        int d = 0;

        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i) > max) {
                max = temp.get(i);
                d = i+1;
            }
        }

        return getMonthName() + " " + d + ": " + max + " degrees";
    }

    public String getMinTemp() {
        float min = MAX_TEMP;
        int d = 0;

        for (int i = 0; i < temp.size(); i++) {
            if (temp.get(i) < min) {
                min = temp.get(i);
                d = i+1;
            }
        }

        return getMonthName() + " " + d + ": " + min + " degrees";
    }

    public String getMonthName() {
        switch (month){
            case 1: return "January";
            case 2: return "February";
            case 3: return "March";
            case 4: return "April";
            case 5: return "May";
            case 6: return "June";
            case 7: return "July";
            case 8: return "August";
            case 9: return "September";
            case 10: return "October";
            case 11: return "November";
            case 12: return "December";

            default:
                return "month is not set";
        }
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getYear() {
        return year;
    }

    @Override
    public String toString() {
        return getMonthName() + " " + getYear();
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
