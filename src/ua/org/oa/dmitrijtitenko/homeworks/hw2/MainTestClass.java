package ua.org.oa.dmitrijtitenko.homeworks.hw2;

public class MainTestClass {
    public static void main(String[] args) {
        Shop wallMarket = new Shop(
                "Wall Market",
                "Big Market",
                "UA, Kharkiv, in the middle of nowhere");

        wallMarket.addGoods(new Goods("Apple","fruit", 7.5f));
        wallMarket.addGoods(new Goods("Grape","fruit", 2.1f));
        wallMarket.addGoods(new Goods("Banana", 3.1f));
        wallMarket.addGoods(new Goods("Potato"));
        wallMarket.addGoods(new Goods("Maracujá", -5f));

        System.out.println(wallMarket);

        wallMarket.printAllGoods();

        System.out.println("Average price in " + wallMarket.getTitle() + " is: "
                + wallMarket.getAvarageGoodsPrice() + " UAH");

        System.out.println();

        System.out.println("Goods which price is less then average: ");
        wallMarket.printGoods_PriceLessThenAverage();

        System.out.println();

        System.out.println("Goods which price is greater then average: ");
        wallMarket.printGoods_PriceGreaterThenAverage();

        //
        float price = 5;

        System.out.println();
        System.out.println("Goods which price is greater then " + price + ": ");
        wallMarket.printGoods_PriceGreaterThen(price);

        System.out.println();
        System.out.println("Goods which price is less then " + price + ": ");
        wallMarket.printGoods_PriceGreaterThen(price);
    }
}
