package ua.org.oa.dmitrijtitenko.homeworks.hw2;

import java.util.ArrayList;
import java.util.List;

public class Shop {

    private String title;
    private String description;
    private String address;

    private List<Goods> goodsList = new ArrayList<>();

    public Shop(String title, String description, String address) {
        setTitle(title);
        setDescription(description);
        setAddress(address);
    }

    public Shop(List<Goods> goodsList) {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void addGoods(Goods item) {
        this.goodsList.add(item);
    }

    @Override
    public String toString() {
        return "Shop{" +
                "title='" + getTitle() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", address='" + getAddress() + '\'' +
                '}';
    }

    public void printAllGoods() {
        for (Goods item:goodsList)
            System.out.println("\t" + item);
    }

    public float getAvarageGoodsPrice() {
        float total = 0;
        for (Goods item:goodsList)
            total += item.getPrice();
        return total/goodsList.size();
    }

    public void printGoods_PriceLessThenAverage(){
        printGoods_PriceLessThen(getAvarageGoodsPrice());
    }

    public void printGoods_PriceGreaterThenAverage(){
        printGoods_PriceGreaterThen(getAvarageGoodsPrice());
    }

    public void printGoods_PriceLessThen(float p){
        for (Goods item:goodsList)
            if(item.getPrice()<p)
                System.out.println(item);
    }

    public void printGoods_PriceGreaterThen(float p){
        for (Goods item:goodsList)
            if(item.getPrice()>p)
                System.out.println(item);
    }
}
