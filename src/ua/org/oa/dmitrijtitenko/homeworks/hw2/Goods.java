package ua.org.oa.dmitrijtitenko.homeworks.hw2;

public class Goods {

    private String title;
    private String description;
    private float price;

    public Goods(String title, String description, float price) {
        setTitle(title);
        setDescription(description);
        setPrice(price);
    }

    public Goods(String title, float price) {
        setTitle(title);
        setDescription("N/A");
        setPrice(price);
    }

    public Goods(String title) {
        setTitle(title);
        setDescription("N/A");
        setPrice(0);
    }

    public Goods() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        if(price >= 0)
            this.price = price;
        else{
            System.out.println("Price can't be negative. " +
                    title + "'s price will be set to 0 UAH");
        }
    }

    @Override
    public String toString() {
        return "Goods{" +
                "title='" + getTitle() + '\'' +
                ", description='" + getDescription() + '\'' +
                ", price=" + getPrice() + "UAH" +
                '}';
    }
}
