package ua.org.oa.dmitrijtitenko.homeworks.hw3;

public class App {
    public static void main(String[] args) {
        TextProcessor proc = new TextProcessor("file.txt");

        System.out.println(proc.print());
        System.out.println("----------");

        int n = 20;
        proc.findSymbols(n);
    }
}
