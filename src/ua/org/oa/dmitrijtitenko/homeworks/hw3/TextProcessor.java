package ua.org.oa.dmitrijtitenko.homeworks.hw3;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class TextProcessor {

    private StringBuilder text = new StringBuilder();   //text from file

    //class constructor read file and initialize text field
    public TextProcessor(String fileName){
        try (BufferedReader br = Files.newBufferedReader(Paths.get( fileName))){
            String b;
            while((b = br.readLine()) != null){
                text.append(b);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //just return loaded text
    public String print(){
        return text.toString();
    }

    //find n most frequently occured symbols
    public void findSymbols(int n) {
        char unique[] = new char[n];
        int cnt[] = new int[n];
        char textArr[] = text.toString().toCharArray();

        unique [0] = textArr[0];
        cnt [0] = 1;

        int uniqueSize = 1;    //unique arr size

        for (int i = 0; i < textArr.length; i++) {
            for (int j = 0; j < unique.length; j++) {

                //if equal break and ++ counter;
                if(textArr[i] == unique[j]){
                    cnt[j]++;
                    break;
                }

                //if last element in unique then add char to unique
                if((unique.length-1) == j && uniqueSize < n){
                    unique[uniqueSize] = textArr[i];
                    cnt[j] = 1;
                    uniqueSize++;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            System.out.println(unique[i] + " " + cnt[i]);
        }
    }
}
