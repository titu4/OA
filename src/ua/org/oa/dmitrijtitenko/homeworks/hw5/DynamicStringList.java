package ua.org.oa.dmitrijtitenko.homeworks.hw5;

public class DynamicStringList implements SimpleList{

    private static final int INCREMENT = 10;
    private String[] arr;
    private int currentId;

    public DynamicStringList() {
        arr = new String[INCREMENT];
        currentId = 0;
    }

    public DynamicStringList(int i) {
        arr = new String[i];
        currentId = 0;
    }

    @Override
    public void add(String s) {
        increaseArrSize();
        arr[currentId++] = s;
    }

    private void increaseArrSize() {
        if(currentId == arr.length ||
                arr.length == 0) {
            String[] arr_ = new String[arr.length + INCREMENT];
            for (int i = 0; i < arr.length; i++) {
                arr_[i] = arr[i];
            }
            arr = arr_;
        }
    }

    @Override
    public String get() {
        return arr[currentId-1];
    }

    @Override
    public String get(int id) {
        return arr[id-1];
    }

    @Override
    public String remove() {
        String str = remove(currentId);
        return str;
    }

    @Override
    public String remove(int id) {
        String str = arr[id-1];
        arr[id-1] = null;
        return str;
    }

    @Override
    public boolean delete() {
        arr = null;
        return true;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("[");

        if (arr != null) {
            for (String s : arr) {
                if (s != null) {
                    str.append(s);
                    str.append(" ");
                }
            }
        }

        if (str.length()>2)
            str.deleteCharAt(str.length()-1); //remove tail whiteSpace

        str.append("]");

        return str.toString();
    }
}
