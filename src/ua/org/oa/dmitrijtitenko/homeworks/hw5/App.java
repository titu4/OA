package ua.org.oa.dmitrijtitenko.homeworks.hw5;

public class App {
    public static void main(String[] args) {
        DynamicStringList helloWorld = new DynamicStringList();

        helloWorld.add("Hello"); //hello
        System.out.println("add: " + helloWorld);

        helloWorld.add("world"); //hello world
        System.out.println("add: " + helloWorld);

        System.out.println("getLast: " + helloWorld.get()); //world
        System.out.println("getFirst: " + helloWorld.get(1)); //world
        System.out.println("removeFirst: " + helloWorld.remove(1));
        System.out.println("afterRemove: " + helloWorld);

        System.out.println("---");

        DynamicStringList helloJava = new DynamicStringList(2);

        helloJava.add("hello"); //hello
        helloJava.add("java"); //hello java
        helloJava.add("world"); //hello java world

        System.out.println("toString: " + helloJava.toString());

        helloJava.delete();
        System.out.println("afterDeleteAll:" + helloJava.toString());
    }
}
