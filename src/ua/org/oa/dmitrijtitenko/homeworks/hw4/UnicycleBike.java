package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class UnicycleBike extends Bicycle {
    private static final int NUMBER_OF_WHEELS = 1;
    private static final int NUMBER_OF_SPEEDS = 1;

    public UnicycleBike(float weight) {
        super(NUMBER_OF_SPEEDS, weight, NUMBER_OF_WHEELS);
    }

    @Override
    public void ride() {
        System.out.println("Enjoy baalance on " + this.getClass().getSimpleName());
    }
}
