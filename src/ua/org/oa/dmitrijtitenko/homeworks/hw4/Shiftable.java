package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public interface Shiftable {

    enum ShifterType {
        front,
        frontRear,
        internal
    }

    int shiftUp();
    int shiftDown();
    int getCurentShisterPosition();
}
