package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class CityBike extends Bicycle implements Shiftable{
    private static final int NUMBER_OF_WHEELS = 2;

    private Shiftable.ShifterType shifterType;
    private int currentGear;

    public CityBike(int speeds, float weight) {
        super(speeds, weight, NUMBER_OF_WHEELS);

        setShifterType(ShifterType.internal);
    }

    @Override
    public int shiftUp() {
        if (currentGear < getGears()) {
            currentGear = +1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted up");
        } else {
            System.out.println("TopGear!");
        }

        return currentGear;
    }

    @Override
    public int shiftDown() {
        if (currentGear > 1) {
            currentGear = -1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted down");
        } else {
            System.out.println(this.getClass().getSimpleName() + " LowerGear!");
        }

        return currentGear;
    }

    @Override
    public int getCurentShisterPosition() {
        return currentGear;
    }

    public void setShifterType(ShifterType type) {
        this.shifterType = type;
    }

    public ShifterType getShifterType() {
        return this.shifterType;
    }

    @Override
    public void ride() {
        System.out.println("Enjoy comfort on " + this.getClass().getSimpleName());
    }
}
