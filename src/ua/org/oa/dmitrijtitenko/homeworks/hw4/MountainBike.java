package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class MountainBike extends Bicycle implements Shiftable{
    private static final int NUMBER_OF_WHEELS = 2;
    private static final int REAR_COGS_COUNT = 11;

    private boolean frontSuspention;
    private boolean rearSuspention;

    private ShifterType shifterType;
    private int currentGear;

    public MountainBike(int speeds, float weight) {
        super(speeds, weight, NUMBER_OF_WHEELS);

        setFrontSuspention(false);
        setRearSuspention(false);

        if(speeds > 1 && speeds <= REAR_COGS_COUNT){
            setShifterType(Shiftable.ShifterType.front);
        } else if (speeds > 11) {
            setShifterType(Shiftable.ShifterType.frontRear);
        }
    }

    public boolean isFrontSuspention() {
        return frontSuspention;
    }

    public void setFrontSuspention(boolean frontSuspention) {
        this.frontSuspention = frontSuspention;
    }

    public boolean isRearSuspention() {
        return rearSuspention;
    }

    public void setRearSuspention(boolean rearSuspention) {
        this.rearSuspention = rearSuspention;
    }

    @Override
    public int shiftUp() {
        if (currentGear < getGears()) {
            currentGear = +1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted up");
        } else {
            System.out.println("TopGear!");
        }

        return currentGear;
    }

    @Override
    public int shiftDown() {
        if (currentGear > 1) {
            currentGear = -1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted down");
        } else {
            System.out.println(this.getClass().getSimpleName() + " LowerGear!");
        }

        return currentGear;
    }

    @Override
    public int getCurentShisterPosition() {
        return currentGear;
    }

    private void setShifterType(ShifterType type) {
        this.shifterType = type;
    }

    private ShifterType getShifterType() {
        return this.shifterType;
    }

    @Override
    public void ride() {
        System.out.println("Have fun on " + this.getClass().getSimpleName());
    }
}
