package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class RoadBike extends Bicycle implements Shiftable{
    private static final int NUMBER_OF_WHEELS = 2;
    private static final int REAR_COGS_COUNT = 10;

    private ShifterType shifterType;
    private int currentGear;

    public RoadBike(int gears, float weight) {
        super(gears, weight, NUMBER_OF_WHEELS);

        if(gears > 1 && gears <= REAR_COGS_COUNT){
            setShifterType(ShifterType.front);
        } else if (gears > 11) {
            setShifterType(ShifterType.frontRear);
        }
    }

    @Override
    public int shiftUp() {
        if (currentGear < getGears()) {
            currentGear = +1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted up");
        } else {
            System.out.println("TopGear!");
        }

        return currentGear;
    }

    @Override
    public int shiftDown() {
        if (currentGear > 1) {
            currentGear = -1;
            System.out.println(this.getClass().getSimpleName() + " gear shifted down");
        } else {
            System.out.println(this.getClass().getSimpleName() + " LowerGear!");
        }

        return currentGear;
    }

    @Override
    public int getCurentShisterPosition() {
        return currentGear;
    }

    private void setShifterType(ShifterType type) {
        this.shifterType = type;
    }

    private ShifterType getShifterType() {
        return this.shifterType;
    }

    public void ride() {
        System.out.println("Get satisfaction from speed on  " + this.getClass().getSimpleName());
    }

}
