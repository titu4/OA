package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class BabyBike extends Bicycle {
    private static final int NUMBER_OF_SPEEDS = 1;

    public BabyBike(float weight, int wheels) {
        super(NUMBER_OF_SPEEDS, weight, wheels);
    }

    @Override
    public void ride() {
        System.out.println("Enjoy your first ride on " + this.getClass().getSimpleName());
    }
}
