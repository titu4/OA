package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class HardTailBike extends MountainBike {

    public HardTailBike(int speeds, float weight) {
        super(speeds, weight);

        setFrontSuspention(true);
        setRearSuspention(false);
    }

}
