package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public class SoftTailBike extends MountainBike {

    public SoftTailBike(int speeds, float weight) {
        super(speeds, weight);

        setFrontSuspention(true);
        setRearSuspention(true);
    }

    @Override
    public void ride() {
        super.ride();
        this.pump();
    }

    private void pump() {
        System.out.println("\t Enjoy pump on " + this.getClass().getSimpleName());
    }
}
