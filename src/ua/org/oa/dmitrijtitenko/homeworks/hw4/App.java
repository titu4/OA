package ua.org.oa.dmitrijtitenko.homeworks.hw4;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class App {

    public static void main(String[] args) {
        Bicycle scottCR1 = new RoadBike(22,7.3f);
        scottCR1.setBrand("Scott").setBikeTitle("CR1");

        Bicycle gnom = new BabyBike(11f,3);
        gnom.setBrand("HVZ").setBikeTitle("Gnom");

        Bicycle mUni = new UnicycleBike(9.4f);
        mUni.setBikeTitle("MountainUnicycle");

        Bicycle steel29er = new MountainBike(11,7.5f);
        steel29er.setBrand("Titu4").setBikeTitle("Steel SS 29er");

        Bicycle scottSUB = new CityBike(11,12f);
        scottSUB.setBrand("Scott").setBikeTitle("SUB3");

        Bicycle carraro = new HardTailBike(22,9.4f);
        carraro.setBrand("Carraro").setBikeTitle("Daytone");

        Bicycle prideCustom = new SoftTailBike(24,10.4f);
        prideCustom.setBrand("Pride").setBikeTitle("pdx-26");

        List<Bicycle> bicycles = new LinkedList<>();

        bicycles.add(scottCR1);
        bicycles.add(gnom);
        bicycles.add(mUni);
        bicycles.add(steel29er);
        bicycles.add(scottSUB);
        bicycles.add(carraro);
        bicycles.add(prideCustom);

        for (Bicycle b:bicycles){
            System.out.println(b.toString());
            b.ride();
        }

        System.out.println("------");

        List<Shiftable> shiftableBikes = new ArrayList<>();
        shiftableBikes.add((Shiftable) scottCR1);
        shiftableBikes.add((Shiftable) carraro);
        shiftableBikes.add((Shiftable) scottSUB);

        for (Shiftable b:shiftableBikes){
            int currentShifterPosiotion;
            b.shiftUp();
            b.shiftUp();
            currentShifterPosiotion = b.shiftUp();

            if (currentShifterPosiotion>5)
                b.shiftUp();
            else
                b.shiftDown();
        }

        System.out.println("-----");

        System.out.println(getBabyBikes(bicycles,3));
        System.out.println(getRoadBikes(bicycles,22));
        System.out.println(getMTBbikes(bicycles,5,10));
    }

    private static ArrayList<BabyBike> getBabyBikes(List<Bicycle> bicycles, int wheelNum) {
        ArrayList<BabyBike> babyBikes = new ArrayList<>();
        System.out.println("Baby bikes with " + wheelNum + " wheels:");
        for (Bicycle b:bicycles){
            try{
                BabyBike bb = (BabyBike) b;
                if(bb.getWheels() == wheelNum) {
                    babyBikes.add(bb);
                }
            }catch (ClassCastException e){}
        }

        return babyBikes;
    }

    private static ArrayList<RoadBike> getRoadBikes(List<Bicycle> bicycles, int gears) {
        ArrayList<RoadBike> roadBikes = new ArrayList<>();
        System.out.println("Road bikes with " + gears+ " gears:");
        for (Bicycle b:bicycles){
            try{
                RoadBike rb = (RoadBike) b;
                if(rb.getGears() == gears) {
                    roadBikes.add(rb);
                }
            }catch (ClassCastException e){}
        }

        return roadBikes;
    }

    private static ArrayList<MountainBike> getMTBbikes(List<Bicycle> bicycles, float lo, float hi) {
        ArrayList<MountainBike> mountainBikes = new ArrayList<>();
        System.out.println("Mountain bikes with weight in range from " + lo + " to " + hi);
        for (Bicycle b:bicycles){
            try{
                MountainBike mb = (MountainBike) b;
                if(lo <= mb.getWeight() && mb.getWeight() <= hi) {
                    mountainBikes.add(mb);
                }
            }catch (ClassCastException e){}
        }

        return mountainBikes;
    }

}
