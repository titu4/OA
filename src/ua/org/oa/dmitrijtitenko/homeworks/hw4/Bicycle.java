package ua.org.oa.dmitrijtitenko.homeworks.hw4;

public abstract class Bicycle {
    private int gears; //number of gears
    private float weight;   //bicycle weight
    private float wheels; //number of wheels

    private String brand = "unknown";
    private String bikeTitle = "unknown";

    protected Bicycle(int gears, float weight, float wheels) {
        setGears(gears);
        setWeight(weight);
        setWheels(wheels);
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getWheels() {
        return wheels;
    }

    public void setWheels(float wheels) {
        this.wheels = wheels;
    }

    @Override
    public String toString() {
        return "Bicycle{" +
                "type=" + this.getClass().getSimpleName() +
                ", brand=" + getBrand() +
                ", title=" + getBikeTitle() +
                ", gears=" + getGears() +
                ", weight=" + getWeight() +
                ", wheels=" + getWheels() +
                '}';
    }

    public String getBrand() {
        return brand;
    }

    public Bicycle setBrand(String brand) {
        this.brand = brand;
        return this;
    }

    public String getBikeTitle() {
        return bikeTitle;
    }

    public Bicycle setBikeTitle(String bikeTitle) {
        this.bikeTitle = bikeTitle;
        return this;
    }

    public abstract void ride();
}
