package ua.org.oa.dmitrijtitenko.homeworks.hw6;

public class NullPointer {
    static protected String[] s;

    public static void raiseException1() {
        try {
            String s = null;
            s.length();
        }catch (NullPointerException ex){
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }
    }

    public static void raiseException2() {
        try {
            throw null;
        } catch (NullPointerException ex) {
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }
    }

    public static void raiseException3() {
        try {
            int l = s.length;
        } catch (NullPointerException ex) {
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }
    }
}
