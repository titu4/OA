package ua.org.oa.dmitrijtitenko.homeworks.hw6;

public class ClassCast {

    public static void raiseException(){
        try {
            Exception e = (ClassCastException) (new Exception());
        }catch (ClassCastException ex){
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }
    }
}
