package ua.org.oa.dmitrijtitenko.homeworks.hw6;

public class StringIndexOutOfBounds {
    public static void raiseException(){
        try{
            String s = new String("hello");
            s.charAt(s.length());
        }catch (StringIndexOutOfBoundsException ex){
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }
    }
}
