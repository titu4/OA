package ua.org.oa.dmitrijtitenko.homeworks.hw6;

public class App {
    public static void main(String[] args) throws InterruptedException {
//        ArrayIndexOutOfBoundsException
        System.out.println("a:");
        ArrayIndexOutOfBounds.raiseException();

//        IllegalArgumentException
        System.out.println("b:");
        IllegalArgument.raiseException(101);

//        ClassCastException
        System.out.println("c:");
        ClassCast.raiseException();

//        StringIndexOutOfBoundsException
        System.out.println("d:");
        StringIndexOutOfBounds.raiseException();

//        NullPointerException
        System.out.println("e1:");
        NullPointer.raiseException1();
        System.out.println("e2:");
        NullPointer.raiseException2();
        System.out.println("e3:");
        NullPointer.raiseException3();

//        StackOverflowException
        System.out.println("f:");
        StackOverflow.raiseError();

//        NumberFormatException
        System.out.println("g:");
        NumberFormat.raiseException();

//        OutOfMemoryException
//        run with -Xmx12m option
        System.out.println("h:");
        OutOfMemory.raiseError();
    }
}
