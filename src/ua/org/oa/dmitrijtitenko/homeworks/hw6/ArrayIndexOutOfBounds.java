package ua.org.oa.dmitrijtitenko.homeworks.hw6;

public class ArrayIndexOutOfBounds {
    public static void raiseException(){
        try {
            int[] arr = {1,2,3};
            arr[5] = 0;
        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println(ex.toString());
//            ex.printStackTrace();
        }

    }
}
