package ua.org.oa.dmitrijtitenko.homeworks.hw1;


public class ControlStatement {

    public static void main(String[] args) {
        System.out.println("1) Square area: " + squareArea(10));

        System.out.println("2) Distance: " + carDistance(100, 80, 200, 1) + "km");

        System.out.println("3) A * x * x + B * x + C = 0 :" + f(1, 1, -2));

        System.out.println("4) " + checkNumber(0));

        System.out.println("5) " + sumOfBiggest( 10, 5, 8));

        System.out.println("6) 5: " + descNum(-5));

        System.out.println("7) A > 2 и B ≤ 3 : " + checkAB(3, 2));

        System.out.println("8) A < B < C : " + checkABC(1, 2, 3));

        System.out.println("9) 1 1 1 2 : " + caseCheck(1, 1, 1, 2));

        System.out.println("10) 5 : " + getRate(5));

        System.out.println("11) 12 : " + rateMonth(12));

        System.out.println("12) A + B : " + doMath(1, 5, 5));

        System.out.println("13) A < B : ");
        getNumbers(3, 5);

        System.out.println("14) sum A B : " + getNumbersSum(3, 5));

        System.out.println("15) N fact : " + fact(5));

        System.out.println("16) A>B : " + countBinA(8, 3));

        System.out.println("17)  3 * K > N : " + minK(9));

        System.out.println("18) print all digits : " );
        printDigits(123);

        System.out.println("19) A<B : ");
        printAB(3,5);
    }

    private static void printAB(int a, int b) {
        int i=1,c=a;
        while(a<=b){
            for(int j=1;j<=i;j++){
                System.out.print(a);
            }

            System.out.println();
            a++;
            i++;
        }

    }

    private static void printDigits(int n) {
        while (n%10>0){
            System.out.println(n%10);
            n/=10;
        }
    }

    private static int minK(int n) {
        int k = 1;
        while(3*k <= n){
            k++;
        }
        return k;
    }

    private static int countBinA(int a, int b) {
        while(a>=b) {
            a -= b;
        }
        return a;
    }

    private static int fact(int n) {
        int f=1;
        for (int i=1 ; i<=n ; i++){
            f = f*i;
        }
        return f;
    }

    private static int getNumbersSum(int a, int b) {
        int sum = 0;
        for (int i=a ; i<=b ; i++){
            sum += i;
        }
        return sum;
    }

    private static void getNumbers(int a, int b) {
        int j=0;
        for (int i = a; i<=b; i++,j++){
            System.out.println("\t" + i);
        }
        System.out.println("\ttotal:" + j);
    }

    private static int doMath(int act, int a, int b) {
        switch (act){
            case 1:
                return a + b;
            case 2:
                return a - b;
            case 3:
                return a * b;
            case 4:
                return a / b;
            default:
                return 0;
        }
    }

    private static String rateMonth(int m) {
        switch (m){
            case 1: case 2: case 12:
                return "winter";

            case 3: case 4: case 5:
                return "spring";

            case 6: case 7: case 8:
                return "summer";

            case 9: case 10: case 11:
                return "fall";

            default:
                return "err";
        }
    }


    private static int squareArea(int a) {
        return a*a;
    }

    private static int carDistance(int v1, int v2, int s, int t) {
        return Math.abs(s - (v1+v2) * t);
    }

    private static String f(int a, int b, int c) {
        double x1;
        double x2;

        x1 = (-b + Math.sqrt(discr(a, b, c))) / (2*a);
        x2 = (-b - Math.sqrt(discr(a, b, c))) / (2*a);

        return "\n\tx1:" + x1 + "\n\t x2:" + x2;
    }
    private static double discr(int a, int b, int c) {
        return (b * b) - (4 * a * c);
    }

    private static int checkNumber(int i) {
        if(i == 0) {
            i = 10;
        }else if (i < 0){
            i-=2;
        }else {
            i+=1;
        }
        return i;
    }

    private static int sumOfBiggest(int i1, int i2, int i3) {
        int a1;

        if (i1 > i2){
            if (i2 > i3){
                a1 = i1 + i2;
            }else {
                a1 = i1 + i3;
            }
        } else {
            if(i1 > i3){
                a1 = i1 + i2;
            }else {
                a1 = i3 + i2;
            }
        }
        return a1;
    }

    private static String descNum(int i) {
        if(i > 0){
//            return "+";
            if(i%2 == 0){
                return "+chet";
            }else {
                return "+nechet";
            }
        }else if (i < 0){
//            return "-";
            if(i%2 == 0){
                return "-chet";
            }else {
                return "-nechet";
            }
        }else {
            return "zero";
        }
    }

    private static boolean checkAB(int a, int b) {
        if(a>2 & b<=3){
            return true;
        }else {
            return false;
        }
    }

    private static boolean checkABC(int a, int b, int c) {
        if (a<b && b<c) {
            return true;
        }else{
            return false;
        }
    }

    private static int caseCheck(int i1, int i2, int i3, int i4) {
        if(i1!=i2 && i2==i3)
            return 1;
        if(i1!=i2 && i1==i3)
            return 2;

        if(i3!=i4 && i1==i4)
            return 3;
        if(i3!=i4 && i1==i3)
            return 4;
        return 0;
    }

    private static String getRate(int i) {
        switch (i) {
            case 1:
                return "very bad";
            case 2:
                return "bad";
            case 3:
                return "not good";
            case 4:
                return "good";
            case 5:
                return "very good";
            default:
                return "error";
        }
    }
}

